require 'rails_helper'

RSpec.describe User, type: :model do
  describe 'by_username scope' do
    let!(:user_1) { create(:user, username: 'user_1') }
    let!(:user_2) { create(:user, username: 'user_1_10') }

    before(:all) {
      @result = User.by_username('user_1')
      @all_users = User.count
    }

    it 'Should return 2 users' do
      expect(@result.count).to eq(2)
    end

    it 'Should not return all users' do
      expect(@result.count).not_to eq(@all_users)
    end
  end
end
